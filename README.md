# UML_SHUN

#### 成员共同任务
     成员具体对程序进行处理的参考步骤： (1) 在Android Developers网站安装Android Studio (2) 下载源代码，并将源代码导入到Android Studio，对代码进行Build，观察下方运行状态，对出现的报错信息进行解读，找到对应行进行错误处理。同时，结合断点调试进行错误处理。 (3) 上述步骤处理完成后，接下来进行Run前的准备。 首先，将一部Android手机用USB数据线连接在电脑上。第二，打开手机开发人员选项中的USB调试。第三，安装手机厂商的手机助手，将Android驱动安装在电脑上，同时在手机端同意一些弹窗命令要求。最后退出手机助手。 (4) 点击Run，出现ADT界面，安卓手机型号和安卓手机Android版本将在第一行显示，然后点击确认，等待一会，手机将显示APP安装界面，经过一系列确定，APP安装完成。 (5) 分别点开桌面上出现的四个APP的图标，进行测试，发现问题，解决问题。
    成员分别根据该项目源代码使用IBM Rational Rose 2007画出自己所选择的UML类型图，并完成自己的课程设计,将UML类型图及课程设计电子版使用Git GUI各自上传到WangShun/UML_SHUN中。

#### 参与贡献成员
小组组长：
王顺（WangShun）：创建仓库创建了WangShun/UML_SHUN，推送了新的 分支 master 到 WangShun/UML_SHUN，邀请成员加入WangShun/UML_SHUN， 并更新README.md，使用Git GUI上传项目源代码，并上传UML构件图以及自己的课程设计电子版。 
小组成员：
张雪萍（zhangxueping）：加入了 WangShun/UML_SHUN，并上传UML协作图以及自己的课程设计电子版。
张梦月（zhangmengyue）：加入了 WangShun/UML_SHUN，并上传UML顺序图以及自己的课程设计电子版。
孟祥凤（Mengxiangfeng）：加入了 WangShun/UML_SHUN，并上传UML活动图以及自己的课程设计电子版。
李先亮（lixianliang1997）：加入了 WangShun/UML_SHUN，并上传UML用例图以及自己的课程设计电子版。
战雅洁（zhanyajie）：加入了 WangShun/UML_SHUN，并上传UML状态图以及自己的课程设计电子版。
鲍海霞（baohaixia）：加入了 WangShun/UML_SHUN，并上传UML类图以及自己的课程设计电子版。

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)